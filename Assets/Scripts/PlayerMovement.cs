﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {
    // Luodaan nopeusmuuttuja pallolle
    public float speed;
    public Text countText;
    public Text winText;

    // luodaan muuttuja fysiikkakomponentille
    private Rigidbody rb;
    private int count;



	// Use this for initialization
	void Start () {
        // haetaan peliobjektin Rigidbody - komponentti
        rb = GetComponent<Rigidbody>();
        count = 0;

        // Kutsutaan setcounttext funktio
        SetCountText();

        winText.text = "";
	}
	
	// Update is called once per physical step
	void FixedUpdate () {
        // Luodaan horizontal niminen muuttuja joka alustetaan input.getaxisin arvolla
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        float jump;

        if (Input.GetKeyDown(KeyCode.Space))
            jump = 10.8f;
        else
            jump = 0;

        // luodaan Vector3 tyypin muuttuja joka alustetaan Vector3 tyypin oliolla
        Vector3 Movement = new Vector3(moveHorizontal, jump, moveVertical);
        // Lisätään fysiikkakomponenttiin voima kerrottuna nopeudella
        rb.AddForce(Movement * speed);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
 
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 12)
        {
            winText.text = "You win good job!";
        }
    }

}
